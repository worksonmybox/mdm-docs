
# <div style="text-align: center">Yubikey Setup for SSH</div> 
<div style="text-align: center">May 17, 2022</div>
<div style="text-align: center">By: Noah Chelliah</div> 

<br/>

## **Overview**

The Yubikey is a hardware authentication device that when combined with the PKCS#11 module allows external applications to communicate with the PIV application running on a YubiKey.

We can use the Yubikey and the PKCS#11 module to authenticate via SSH to servers. Having the key stored on the Yubikey as opposed to the computer prevents the key from being stolen. Additionally, when we change to a new computer, we don't need to re-generate the SSH key.

We'll set the pin of the Yubikey which will follow the two factor principle of "Something we have" (Yubikey) and "something we know" (the pin). After the pin is set we will generate a certificate with a touch policy that requires the user to be present at the time of the request, self-sign that certificate, and finally import the certificate onto the Yubikey.

There are only two things required for the client machine to authenticate using the Yubikey:

* The opensc package
* A single line added to the /etc/ssh_config file

Once setup, the only configuration required on the server is the public SSH key added to the .ssh/authorized_keys file.


## **Setting Up The Yubikey**

Step 1. Install the OpenSC Agent

```bash
sudo apt-get install opensc
```



Step 2.  Add the repository for the Yubico Software

```bash
sudo apt-add-repository ppa:yubico/stable
```


Step 3. Install the PIV tool which we will later use to provision the Yubikey

```bash
sudo apt-get install yubico-piv-tool
```



Step 4. Use the PIV tool to change the pin from the default '123456' to a pin of your choice. "Pins" are not limited to numbers. You can use a secure password to increase security.

```bash
yubico-piv-tool -a change-pin -P 123456 -N TheNewPinHere
```


Step 5. Generate a certificate 

```bash
yubico-piv-tool -s 9a -a generate --touch-policy=always -o public.pem
```


Step 6. Self Sign the Certificate

```bash
yubico-piv-tool -a verify-pin -P 123456 -a selfsign-certificate -s 9a \
-S "/CN=SSH key/" -i public.pem -o cert.pem
```


Step 7. Import the self-signed certificate

```bash
yubico-piv-tool -a import-certificate -s 9a -i cert.pem
```

</br>
</br>


## **Setting Up Your SSH Client To Work with a Yubikey**

Step 1. Add the PKCS#11 provider to the '''ssh_config''' file

```bash
echo "PKCS11Provider /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so" | sudo tee -a /etc/ssh/ssh_config
```

Step 2. Display the SSH Public key to be stored in the authorized_keys file on remote servers

```bash
ssh-keygen -D /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so
```


