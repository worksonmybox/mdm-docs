# Setting Up Mumble

### Install Mumble

https://www.mumble.info/downloads/

=== ":material-linux: Flatpak"

    ```bash
    flatpak install flathub info.mumble.Mumble
    ```

=== ":material-debian: Debian/Ubuntu Based"

    ```bash
    sudo add-apt-repository ppa:mumble/release
    sudo apt-get update
    sudo apt-get install mumble
    ```

=== ":material-arch: Arch"

    ```bash
    sudo pacman -S mumble
    ```

=== ":material-android: Android"

    [F-Droid](https://f-droid.org/packages/se.lublin.mumla/)
    
    [Google Play](https://play.google.com/store/apps/details?id=se.lublin.mumla&hl=en_US&gl=US)

=== ":fontawesome-brands-fedora: Fedora"

    ```bash
    sudo dnf install mumble
    ```

### Configuring Mumble

After you start mumble you will be greeted by a setup wizard. It will walk you through setting up selecting your microphone and voice gate.

For now close the Server menu. We are going to setup push to talk before we connect to the Mind Drip Media server. This is courteous to others as well as protecting your privacy while we are on the air.

Open your settings panel **Configure -> Settings -> Audio Input** Under Transmission, change **Voice Activity** to **Push To Talk**.

Now that we have push to talk enabled we need to set a hotkey to activate it. Click on **Shortcuts** in the far left panel. Then click on **Add** then on **Unassigned**, scroll all the way down you should see **Push-to-Talk**. Select that option. Click in the black space to the right of **Push To Talk** under the **Shortcut** row. Now press the key you want to be your activation key. We suggest the **Right Control** key. 

### Connecting to the server

Now we are ready to connect to the mumble server.
Close out the settings panel and click on **Server -> Connect**. Hit **Add New**

| Field     | Value                  |
| :-------- | :--------------------- |
| Address   | mumble.minddripone.com |
| Port      | 64738                  |
| Username  | Your User Name         |
| Label     | Mind Drip Media Mumble |


Last but not least Click on **Ok** and double click **Mind Drip Media Mumble** in the servers list.
